from .PV_Optimized_Simulator import Optimized_Simulator


class TestOptimized_Simulator:
    simulator = Optimized_Simulator()

    def test_range(self):
        for _ in range(10000):
            power = self.simulator.generate()
            assert 0 <= power <= 3.250

    def test_peak_hour(self):
        assert self.simulator.generate(14) > 3
        assert self.simulator.generate(14.5) > 3
        assert self.simulator.generate(15) > 3
        assert self.simulator.generate(12.5) > 3

    def test_lowest_hour(self):
        assert self.simulator.generate(5.4) == 0

    def test_illegal_hours(self):
        assert self.simulator.generate(-10) == 0
        assert self.simulator.generate(-0.01) == 0
        assert self.simulator.generate(24.1) == 0
        assert self.simulator.generate(1000000000) == 0
        assert self.simulator.generate(-3.14159) == 0
        assert self.simulator.generate(42) == 0
