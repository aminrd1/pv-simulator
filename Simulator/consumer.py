"""Message consumer
"""
import csv
import logging
import os
from datetime import datetime
from .config import Configuration
from .message import Message
from .PV_Simulator import Simulator

logger = logging.getLogger(__name__)


class Consumer(object):
    """
    Message consumer.

    This class contains the callback method which consumes the received
    meter messages.
    """

    def __init__(self, simulator: Simulator, config: Configuration) -> None:
        self._simulator = simulator
        self._config = config

    def on_power_consumption_received(self, channel, method, properties, body):
        try:
            message = Message()
            message.deserialize(body)
            logger.debug(f'received message: {body}')
            self._write_csv(message)
        except Exception as e:
            logger.exception(e)

    def _write_csv(self, message: Message) -> None:
        """
        Adds a line to a csv file, in the csv path given in the configuration.
        The file name will be the date, e.g. `25-01-2021.csv`.
        """
        csv_row = self._create_csv_row(message)
        csv_file_name = f'{self._date_string(message)}.csv'
        csv_file_path = os.path.join(self._config.csv_path, csv_file_name)
        if not os.path.isfile(csv_file_path):
            with open(csv_file_path, 'w', newline='') as file:
                writer = csv.DictWriter(file, csv_row.keys())
                writer.writeheader()
                writer.writerow(csv_row)
        else:
            with open(csv_file_path, 'a', newline='') as file:
                writer = csv.DictWriter(file, csv_row.keys())
                writer.writerow(csv_row)

    def _create_csv_row(self, message: Message) -> dict:
        """
        Creates a dictionary of the data to be saved in the csv
        """
        generated_PV = None
        timestamp = message.timestamp
        if timestamp is None:
            timestamp = datetime.now().strftime(self._config.timestamp_format)
            generated_PV = self._simulator.generate()
        else:
            hour = self._timestamp_to_hour(timestamp)
            generated_PV = self._simulator.generate(hour)

        from_grid = max(message.power_consumption - generated_PV, 0)
        to_grid = max(generated_PV - message.power_consumption, 0)

        csv_row = {
            "timestamp": timestamp,
            "meter power": message.power_consumption,
            "PV power": generated_PV,
            "meter + power": message.power_consumption + generated_PV,
            "from the grid: max(meter - power, 0)": from_grid,
            "to the grid: max(power - meter, 0)": to_grid
        }
        return csv_row

    def _date_string(self, message: Message):
        if message.timestamp is None:
            date = datetime.now()
        else:
            date = datetime.strptime(message.timestamp,
                                     self._config.timestamp_format)
        return date.strftime("%Y-%m-%d")

    def _timestamp_to_hour(self, timestamp: str) -> float:
        time = datetime.strptime(timestamp, self._config.timestamp_format)
        hour = (time.hour + time.minute/60 +
                time.second/3600 + time.microsecond/3.6e9)
        return hour
