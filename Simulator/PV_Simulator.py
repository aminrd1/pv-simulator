"""PV Simulator
"""
from .PV_Optimized_Simulator import Optimized_Simulator
from numpy.random import normal


class Simulator(Optimized_Simulator):
    """
    A PV Simulator

    It uses the optimized simulated values to generate random PV power values.
    """

    def __init__(self):
        pass

    def generate(self, hour: float or None = None) -> float:
        """
        Returns random simulated PV power in kW based on the time of the day.
        """
        maximum_power = super().generate(hour)
        generated_power = maximum_power * abs(1 - abs(normal(0, 0.4)))
        return generated_power
