import json


class Message(object):
    power_consumption = None
    timestamp = None

    def __init__(self) -> None:
        pass

    def deserialize(self, string: str) -> dict:
        data = json.loads(string)
        self.power_consumption = data['power_consumption']
        if 'timestamp' in data:
            self.timestamp = data['timestamp']
        return data

    def get_dict(self) -> dict:
        data = {
            "power_consumption": self.power_consumption
        }
        if self.timestamp is not None:
            data['timestamp'] = self.timestamp
        return data

    def serialize(self, data: dict or None = None) -> str:
        if data is None:
            data = self.get_dict()
        return json.dumps(data)
