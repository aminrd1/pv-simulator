"""Configuration reader
"""
import json
from pathlib import Path


class Configuration(object):
    """
    Reads json config for Simulator.
    """

    timestamp_format = None
    csv_path = None
    pika_parameters = None
    pika_credentials = None
    pika_queue = None
    logging_create = None
    logging_file_path = None

    _config_file_path = "simulation_config.json"

    def __init__(self) -> None:
        with open(self._config_file_path, 'r') as file:
            config = json.load(file)
        self.timestamp_format = config['timestampFormat']
        self.csv_path = './csv'
        self.pika_parameters = config['pika']['parameters']
        self.pika_credentials = config['pika']['credentials']
        self.pika_queue = config['pika']['queue']
        self.logging_file_path = config['logging']['path']
        self._create_log_dir()

    def _create_log_dir(self) -> None:
        log_path = Path(self.logging_file_path).parent
        print('creating log path: ', log_path)
        log_path.mkdir(parents=True, exist_ok=True)
