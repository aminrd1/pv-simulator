"""PV Optimized Simulator
"""
from datetime import datetime


class Optimized_Simulator(object):
    """
    An optimized PV Simulator.

    It uses current time to generate Optimized PV power
    values (the highest pissible values).
    """

    def __init__(self):
        pass

    def _now_in_hour(self) -> float:
        """
        Returns current time in hour-only format. For example at 04:30 AM and
        06:15 PM it respectively returns 4.5 and 6.25.
        """
        current_time = datetime.now()
        hour = (current_time.hour + current_time.minute/60 +
                current_time.second/3600 + current_time.microsecond/3.6e9)
        return hour

    def generate(self, hour: float or None = None) -> float:
        """
        Returns highest simulated PV power in kW based on the time of the
        day.
        """
        if hour is None:
            hour = self._now_in_hour()
        return self._maximum_generated_power(hour)

    def _maximum_generated_power(self, hour: float) -> float:
        """
        Returns the maximum possible PV power value (in kW) for the given hour.
        :hour: a float representation of hour of the day, for example
        4.5 is 04:30 AM and 18.25 is 06:15 PM.
        """
        if hour < 5.5 or hour >= 20.698:
            return 0
        elif hour < 8:
            return 0.13*hour-0.7107
        elif hour > 20:
            return -0.143*hour+2.96
        elif hour < 14:
            a = -0.0813885
            b = -14
            c = 3.25
            return a*(hour+b)**2+c
        a = -0.08779
        b = -14
        c = 3.25
        return a*(hour+b)**2+c
