"""Main script for the PV Simulator
"""
import logging
import os
import sys
import threading
from time import sleep

from .config import Configuration
from .consumer import Consumer
from .PV_Simulator import Simulator
from .receiver import Receiver


def main(config: Configuration) -> None:
    simulator = Simulator()
    receiver = Receiver(config)
    consumer = Consumer(simulator, config)

    receiver.set_callback(consumer.on_power_consumption_received)

    consumer_thread = threading.Thread(target=receiver.consume)
    consumer_thread.start()

    while True:
        sleep(5)
        print("I am alive, doing cool stuff! (not blocked by RabbitMQ)")


if __name__ == "__main__":
    config = Configuration()
    log_file = config.logging_file_path
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(name)-12s %(message)s',
        filename=log_file,
        filemode='a',
        level=logging.INFO)
    logger = logging.getLogger(__name__)
    logger.info('Started the logging system')

    try:
        main(config)
    except KeyboardInterrupt:
        logger.info('Interrupted by keyboard')
        print('Interrupted by keyboard')

    except Exception as e:
        logger.exception(e)

    finally:
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
