"""Receives messages from RabbitMQ
"""
import pika
from .config import Configuration


class Receiver(object):
    """
    Receives messages from RabbitMQ

    This class accepts RabbitMQ connection parameters and the queue name,
    and receives string data from that queue. It calls a given callback with
    the received data.
    Note: It is better to call the `consume` method in a thread in order not to
    block the caller.

    TODO: it does not stop without keyboard interaction or killing the process
    """

    def __init__(self, config: Configuration) -> None:
        """
        Initializes the Sender by opening the connection and defining the
        queue.
        """
        pika_credentials = pika.PlainCredentials(**config.pika_credentials)
        self._pika_connection = pika.BlockingConnection(
            pika.ConnectionParameters(**config.pika_parameters,
                                      credentials=pika_credentials))
        self._channel = self._pika_connection.channel()
        self._queue = config.pika_queue
        self._channel.queue_declare(queue=self._queue, durable=True,
                                    auto_delete=True)

    def set_callback(self, callback) -> None:
        """
        :callback: is called on receiving each message. It should be defined
        as follows, based on pika documentation:
            def callback(channel, method, properties, body):
                pass
        """
        self._channel.basic_consume(
            queue=self._queue,
            on_message_callback=callback,
            auto_ack=True)

    def consume(self) -> None:
        """
        Blocks the caller and starts receiving messages. On each message the
        `callback` method is called.
        """
        self._channel.start_consuming()

    def disconnect(self) -> None:
        self._pika_connection.close()

    @property
    def connected(self) -> bool:
        return self._pika_connection.is_open

    def sleep(self, duration: float) -> None:
        self._pika_connection.sleep(duration)
