from .config import Configuration
from .meter import Meter
from .sender import Sender


class TestMeter:
    meter = Meter()

    def test_range(self):
        for _ in range(10000):
            power = self.meter.generate()
            assert 0 <= power <= 9


class TestSenderConfig:
    config = Configuration("meter_config_for_test.json")
    sender = None

    def test_connect(self):
        sender = Sender(self.config)
        assert sender.connected
        sender.disconnect()
