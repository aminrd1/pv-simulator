"""Meter: home power consumption simulator
"""
from random import uniform


class Meter(object):
    """
    Home power consumption meter.

    This class mocks a regular home power consumption.
    """

    def __init__(self):
        pass

    def generate(self) -> float:
        """
        generates the home power consumption by kW
        """
        return uniform(0, 9)
