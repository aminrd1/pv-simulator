"""Main script for the Meter
"""
import logging
from datetime import datetime, timedelta, date, time
from numpy.random import normal
from .config import Configuration
from .message import Message
from .meter import Meter
from .sender import Sender


def one_day_fast_simulation(sender: Sender,
                            meter: Meter,
                            timestamp_format: str) -> None:
    message = Message()
    today = date.today()
    seconds = 0
    seconds_in_a_day = 24*60*60

    wait_between_sendings = abs(normal(5, 1))
    seconds += wait_between_sendings

    while seconds < seconds_in_a_day:
        date_object = (datetime.combine(today, time.min) +
                       timedelta(seconds=seconds))
        message.timestamp = date_object.strftime(timestamp_format)
        message.power_consumption = meter.generate()
        sender.send(message.serialize())
        wait_between_sendings = abs(normal(5, 1))
        seconds += wait_between_sendings

    sender.disconnect()


def real_meter_simulation(sender: Sender, meter: Meter) -> None:
    message = Message()
    while True:
        wait_between_sendings = abs(normal(5, 1))
        sender.sleep(wait_between_sendings)
        message.power_consumption = meter.generate()
        sender.send(message.serialize())


if __name__ == "__main__":
    config = Configuration()
    log_file = config.logging_file_path
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(name)-12s %(message)s',
        filename=log_file,
        filemode='a',
        level=logging.INFO)
    logger = logging.getLogger(__name__)
    logger.info('Started the logging system')

    sender = None
    try:
        meter = Meter()
        sender = Sender(config)
        if config.real_meter:
            real_meter_simulation(sender, meter)
        else:
            one_day_fast_simulation(sender, meter, config.timestamp_format)

    except Exception as e:
        logger.exception(e)
    finally:
        if sender is not None and sender.connected:
            sender.disconnect()
