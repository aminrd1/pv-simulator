"""Sends messages to RabbitMQ
"""
import pika
from .config import Configuration


class Sender(object):
    """
    Sends messages to RabbitMQ

    This class accepts RabbitMQ connection parameters and the queue name,
    and sends the string data to that queue
    """

    def __init__(self, config: Configuration) -> None:
        """
        Initializes the Sender by opening the connection and defining the
        queue.
        """
        pika_credentials = pika.PlainCredentials(**config.pika_credentials)
        self._pika_connection = pika.BlockingConnection(
            pika.ConnectionParameters(**config.pika_parameters,
                                      credentials=pika_credentials))
        self._channel = self._pika_connection.channel()
        self._queue = config.pika_queue
        self._channel.queue_declare(queue=self._queue, durable=True,
                                    auto_delete=True)

    def send(self, message: str) -> None:
        self._channel.basic_publish(exchange='', routing_key=self._queue,
                                    body=message)

    def disconnect(self):
        self._pika_connection.close()

    @property
    def connected(self) -> bool:
        return self._pika_connection.is_open

    def sleep(self, duration: float) -> None:
        self._pika_connection.sleep(duration)
