"""Configuration reader
"""
import json
from pathlib import Path


class Configuration(object):
    """
    Reads json config for Meter.
    """

    real_meter = None
    timestamp_format = None
    pika_parameters = None
    pika_credentials = None
    pika_queue = None
    logging_create = None
    logging_file_path = None

    _config_file_path = "meter_config.json"

    def __init__(self, config_path: str or None = None) -> None:
        if config_path is not None:
            self._config_file_path = config_path

        with open(self._config_file_path, 'r') as file:
            config = json.load(file)

        self.real_meter = config['realMeter']
        self.timestamp_format = config['timestampFormat']
        self.pika_parameters = config['pika']['parameters']
        self.pika_credentials = config['pika']['credentials']
        self.pika_queue = config['pika']['queue']
        self.logging_file_path = config['logging']['path']
        self._create_log_dir()

    def _create_log_dir(self) -> None:
        log_path = Path(self.logging_file_path).parent
        log_path.mkdir(parents=True, exist_ok=True)
